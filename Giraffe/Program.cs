﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Giraffe
{
    class Program
    {
        static void Main(string[] args)
        {

            //IO
            string dummyLines = "This is the first line." + Environment.NewLine +
                "This is the second line." + Environment.NewLine +
                "This is the third line.";
            //open file and append lines
            File.AppendAllLines(@"C:\DummyFilex.txt", dummyLines.Split(Environment.NewLine.ToCharArray()).ToList<string>());
            File.AppendAllText(@"C:\DummyFilex.txt", "This is a test!");
            File.WriteAllText(@"C:\DummyFilex.txt", "This one bam die!");

            bool isFileExist = File.Exists(@"C:\DummyFilex.txt");
            //File.Copy(@"C:\DummyFilex.txt", @"C:\Users\victor.mengwa\Desktop\DummyNew.txt");

            DateTime lastAccessed = File.GetLastAccessTime(@"C:\DummyFilex.txt");
            DateTime lastModified = File.GetLastWriteTime(@"C:\DummyFilex.txt");

            Console.WriteLine(lastAccessed);
            Console.WriteLine(lastModified);

            File.Delete(@"C:\Users\victor.mengwa\Desktop\DummyNew.txt");

            Chef chef = new Chef();
            chef.MakeSpecialDish();

            IgboChef igbochef = new IgboChef();
            igbochef.MakeSpecialDish();

            //guessing game
            string secretWord = "Victor";
            string guess = "";
            int guessCount = 0;
            int guessLimit = 3;
            bool outOfGuesses = false;

            while(guess != secretWord && !outOfGuesses)
            {
                if (guessCount < guessLimit)
                {
                    Console.Write("Enter guess: ");
                    guess = Console.ReadLine();
                    guessCount++;
                }
                else
                {
                    outOfGuesses = true;
                }
                
            }
            if (outOfGuesses)
            {
                Console.WriteLine("You Loose!");
            }
            else
            {
                Console.Write("You Win!");
            }

            


            
            Console.WriteLine(GetDay(0));

            //calculator
            Console.Write("Enter a number: ");
            double num1 = Convert.ToDouble(Console.ReadLine());

            Console.Write("Enter Operator: ");
            string ops = Console.ReadLine();

            Console.Write("Enter a number: ");
            double num2 = Convert.ToDouble(Console.ReadLine());

            if (ops == "+")
            {
                Console.WriteLine(num1 + num2);
            }else if(ops == "-"){
                Console.WriteLine(num1 - num2);
            }
            else if (ops == "/")
            {
                Console.WriteLine(num1 / num2);
            }
            else if (ops == "*")
            {
                Console.WriteLine(num1 * num2);
            }
            else
            {
                Console.WriteLine("Invalid Operator");
            }

            //2D Array
            int[,] numberGrid =
            {
                {1,2 },
                {3,4 },
                {5,6 }
            };

            Console.WriteLine(numberGrid[1, 1]);

            int[,] myArray = new int[2, 3];
            Console.WriteLine();



            //class objects
            //instance of a book
            Book book1 = new Book("Fire and Blood", "Ronin Jewels", 2344) ;
           
            Book book2 = new Book("Harry Potter", "JK Rowlings", 1290);

            book2.title = "The Hobbit";

            Console.WriteLine(book1.title);

            //getters and setters
            Movie avaengers = new Movie("Avengers", "Jeff Bezos", "PG-13");
            Movie shrek = new Movie("Shrek", "Brandon Iyke", "PG");

            Console.WriteLine(avaengers.Rating);

            Song notAfraid = new Song("Not Afraid", "Eminem", 200);
            Song lonely = new Song("Lonely", "Akon", 300);

            Console.WriteLine(Song.songCount);

            Console.ReadLine();
        }

        //get day from integer
        static string GetDay(int dayNum)
        {
            string dayName;

            switch (dayNum)
            {
                case 0:
                    dayName = "Sunday";
                    break;
                case 1:
                    dayName = "Monday";
                    break;
                case 2:
                    dayName = "Tuesday";
                    break;
                case 3:
                    dayName = "Wednesday";
                    break;
                case 4:
                    dayName = "Thursday";
                    break;
                case 5:
                    dayName = "Friday";
                    break;
                case 6:
                    dayName = "Saturday";
                    break;
                default:
                    dayName = "Invalid day number";
                    break;

            }
            return dayName;
        }

        enum WeekDays
        {
            Monday,
            Tuesday,
            Wednesday,
            Thursday,
            Friday,
            Saturday,
            Sunday
        }
    }
}
