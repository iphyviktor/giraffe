﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Giraffe
{
    class Chef
    {
        public void MakeEgusi()
        {
            Console.WriteLine("Chef is making Egusi soup");
        }

        public void MakeOfada()
        {
            Console.WriteLine("Chef is making Ofada sauce");
        }

        public void MakeOgbono()
        {
            Console.WriteLine("Chef is making Ogbono");
        }

        public virtual void MakeSpecialDish()
        {
            Console.WriteLine("Chef is making Egusi soup");
        }
    }
}
